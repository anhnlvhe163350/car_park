import { NextResponse } from "next/server";
import { main } from "../route";
import prisma from "@/src/app/lib/prisma";
export const GET = async (req: Request, res: NextResponse) =>{
    try {
        const licensePlate = req.url.split("/car/")[1];
        
        await main();
        const car = await prisma.car.findUnique({where: {licensePlate} });
        if(!car)
            return NextResponse.json({message: "Not Found"}, {status: 404});
        return NextResponse.json({message: "Success", car}, {status: 200});
    } catch (error) {
        return NextResponse.json({message: "Error", error}, {status: 500});
    }finally{
        await prisma.$disconnect();
    }
};

export const PUT = async (req: Request, res: NextResponse) => {
    try {
        const licensePlate = req.url.split("/car/")[1];
        const {
            carColor,
            carType,
            company,
            parkId } = await req.json(); 
        
        await main();
        const car = await prisma.car.update({
            data: {licensePlate,
                carColor,
                carType,
                company,
                parkId },
            where: {licensePlate}
        })
        // const post = await prisma.post.findFirst({where: {id: parseInt(id)} });
        // if(!post)
        //     return NextResponse.json({message: "Not Found"}, {status: 404});
         return NextResponse.json({message: "Success", car}, {status: 200});
    } catch (error) {
        return NextResponse.json({message: "Error", error}, {status: 500});
    }finally{
        await prisma.$disconnect();
    }
};

export const DELETE = async (req: Request, res: NextResponse) => {
    try {
        const licensePlate = req.url.split("/car/")[1];
        const {
            carColor,
            carType,
            company,
            parkId} = await req.json(); 
        
        await main();
        const car = await prisma.car.delete({where: {licensePlate} });
        // const post = await prisma.post.findFirst({where: {id: parseInt(id)} });
        // if(!post)
        //     return NextResponse.json({message: "Not Found"}, {status: 404});
         return NextResponse.json({message: "Success", car}, {status: 200});
    } catch (error) {
        return NextResponse.json({message: "Error", error}, {status: 500});
    }finally{
        await prisma.$disconnect();
    }
};


