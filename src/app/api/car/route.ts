import prisma from "../../lib/prisma";
import { NextResponse } from "next/server";

export async function main() {
    try {
        await prisma.$connect();
    } catch (error) {
        return Error("Database Connection Unsuccessful")   
    }
}

export const GET = async (req: Request, res: NextResponse) => {
    try {
        await main();
        const cars = await prisma.car.findMany();
        return NextResponse.json({message: "Success", cars}, {status: 200});
    } catch (error) {
        return NextResponse.json({message: "Error", error}, {status: 500});
    } finally{
        await prisma.$disconnect();
    }
}

export const POST = async (req: Request, res: NextResponse) => {
    try {
        const{licensePlate,
            carColor,
            carType,
            company,
            parkId
            } = await req.json();
        await main();
        const car = await prisma.car.create({data:{licensePlate,
            carColor,
            carType,
            company,
            parkId}});
        return NextResponse.json({message: "Success", car}, {status: 201});
    } catch (error) {
        console.log(error);
        return NextResponse.json({message: "Error", error}, {status: 202});
    } finally{
        await prisma.$disconnect();
    }
    


}