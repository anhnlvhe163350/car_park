import prisma from "../../lib/prisma";
import { NextResponse } from "next/server";

export async function main() {
    try {
        await prisma.$connect();
    } catch (error) {
        return Error("Database Connection Unsuccessful")   
    }
}

export const GET = async (req: Request, res: NextResponse) => {
    try {
        await main();
        const employees = await prisma.employee.findMany();
        return NextResponse.json({message: "Success", employees}, {status: 200});
    } catch (error) {
        return NextResponse.json({message: "Error", error}, {status: 500});
    } finally{
        await prisma.$disconnect();
    }
}

export const POST = async (req: Request, res: NextResponse) => {
    try {
        const{
            account, 
            department, 
            employeeAddress, 
            employeeBirthdate,  
            employeeEmail,
            employeeName,
            employeePhone,
            passwordHash, 
            sex } = await req.json();
        await main();
        const employee = await prisma.employee.create({data:{sex,
             passwordHash, employeePhone, employeeName, employeeEmail, employeeBirthdate, employeeAddress, department, account}});
        return NextResponse.json({message: "Success", employee}, {status: 201});
    } catch (error) {
        console.log(error);
        return NextResponse.json({message: "Error", error}, {status: 202});
    } finally{
        await prisma.$disconnect();
    }
    


}