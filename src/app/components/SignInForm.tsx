"use client";

import React, { useState } from "react";
import { signUp } from "../action/users/signUp";
import { useSession } from "next-auth/react";

const SignInForm = () => {

    const {status} = useSession();

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')

    const handleSubmit = async () => {
        setMessage("Signing in...");
        const message = await signUp(email, password);
        setMessage(message);
    };
    
    return(
        
    <div className="grid place-items-center h-screen">
    <div className="shadow-lg p-8 bg-zinc-300/10 flex flex-col gap-2 my-6">
    <div><input type="text" value={email} onChange={(e) => setEmail(e.target.value)}/></div>
    <div><input type="password"value={password} onChange={(e) => setPassword(e.target.value)}/></div>
    <button onClick={handleSubmit} className="bg-red-500 text-while px-6 py-2 mt-3">Sign In</button>
    <p>
                {message}
            </p>
</div>
</div>

    )
}

export default SignInForm;